#!/bin/bash

cd src

echo "Compressing *.js and *.css files"

PACKED_CSS=css/packed.css
PACKED_JS=js/packed.js
rm -rf $PACKED_CSS $PACKED_JS

for file in bootstrap.min.css animate.css font-awesome.min.css style.css; do cat css/${file} >> $PACKED_CSS; rm css/${file}; done
for file in jquery.js bootstrap.min.js jquery.backstretch.min.js smoothscroll.js wow.min.js custom.js; do cat js/${file} >> $PACKED_JS; rm js/${file}; done

PACKED_CSS="<link rel=\"stylesheet\" href=\"${PACKED_CSS}\">"
PACKED_JS="<script src=\"${PACKED_JS}\"></script>"

sed -i ":a;N;\$!ba;s@<!--CSS-->.*<!--/CSS-->@${PACKED_CSS}@g" index.html
sed -i ":a;N;\$!ba;s@<!--SCRIPTS-->.*<!--/SCRIPTS-->@${PACKED_JS}@g" index.html

ls -1 css | xargs -I{} gzip -9 css/{}
ls -1 js | xargs -I{} gzip -9 js/{}
find -type f -name '*.gz' | while read f; do mv "$f" "${f%.*}"; done

echo "Adding current timestamp"
now=`date +"%s"`
find * -type f \( -name '*.css' -o -name '*.js' \) | while read f; do old=$f; new="${f%.*}.$now.${f##*.}"; mv "$old" "$new"; sed -i "s@${old}@${new}@g" index.html; done

echo "Compressing index.html file"
gzip -9 index.html
ls -1 *.gz | while read f; do mv ${f} ${f%.*} ;done

echo "Sending files to S3 bucket '${S3_BUCKET}'"
aws s3 sync . s3://${S3_BUCKET} --exclude 'css/*' --exclude 'js/*' --exclude 'fonts/*' --exclude 'index.html' --cache-control 'max-age=86400'
aws s3 cp index.html s3://${S3_BUCKET} --content-encoding 'gzip'
aws s3 sync css s3://${S3_BUCKET}/css --content-encoding 'gzip' --cache-control 'max-age=32140800'
aws s3 sync js s3://${S3_BUCKET}/js --content-encoding 'gzip' --cache-control 'max-age=32140800'
aws s3 sync fonts s3://${S3_BUCKET}/fonts --cache-control 'max-age=32140800'