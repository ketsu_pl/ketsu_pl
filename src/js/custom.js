(function ($) {

    "use strict";

        // PRE LOADER
        $(window).load(function(){
          $('.preloader').fadeOut(300); // set duration in brackets    
        });


        // MENU
        $('.navbar-collapse a').on('click',function(){
          $(".navbar-collapse").collapse('hide');
        });


        // BACKSTRETCH SLIDESHOW
        $("#home").backstretch([
          "images/home-bg-slideshow-image1.webp"
          ], {duration: 2000, fade: 750});
        

        // SMOOTHSCROLL
        $(function() {
          $('.navbar-default a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 49
            }, 1000);
            event.preventDefault();
          });
        });  


        // WOW ANIMATION
        new WOW({ mobile: false }).init();

})(jQuery);
